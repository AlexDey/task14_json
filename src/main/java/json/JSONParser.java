package json;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Firearm;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

class JSONParser {
    private ObjectMapper objectMapper;

    JSONParser() {
        this.objectMapper = new ObjectMapper();
    }

    List<Firearm> getFirearmList(File jsonFile){
        Firearm[] firearms = new Firearm[0];
        try{
            firearms = objectMapper.readValue(jsonFile, Firearm[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Arrays.asList(firearms);
    }
}
