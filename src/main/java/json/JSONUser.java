package json;

import model.Firearm;

import java.io.File;
import java.util.Comparator;
import java.util.List;

public class JSONUser {

    public static void main(String[] args) {
        File json = new File("src/main/resources/firearm.json");
        File schema = new File("src/main/resources/firearmSchema.json");

        // check json file using json schema doc
        System.out.println(JSONSchemeValidator.validateJSON(json,schema));

        JSONParser parser = new JSONParser();

        List<Firearm> list = parser.getFirearmList(json);

        // sort objects in list by 'origin' field
        list.sort(Comparator.comparing(Firearm::getOrigin));

        // print list
        printList(list);
    }

    private static void printList(List<Firearm> firearms) {
        System.out.println("JSON");
        for (Firearm firearm : firearms) {
            System.out.println(firearm);
        }
    }
}
