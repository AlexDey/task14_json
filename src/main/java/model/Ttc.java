package model;

public class Ttc {

    private int generalRange;
    private int sightRange;
    private boolean cartridge;
    private boolean optics;

    public Ttc() {
    }

    public Ttc(int generalRange, int sightRange, boolean cartridge, boolean optics) {
        this.generalRange = generalRange;
        this.sightRange = sightRange;
        this.cartridge = cartridge;
        this.optics = optics;
    }

    public int getGeneralRange() {
        return generalRange;
    }

    public void setGeneralRange(int generalRange) {
        this.generalRange = generalRange;
    }

    public int getSightRange() {
        return sightRange;
    }

    public void setSightRange(int sightRange) {
        this.sightRange = sightRange;
    }

    public boolean isCartridge() {
        return cartridge;
    }

    public void setCartridge(boolean cartridge) {
        this.cartridge = cartridge;
    }

    public boolean isOptics() {
        return optics;
    }

    public void setOptics(boolean optics) {
        this.optics = optics;
    }

    @Override
    public String toString() {
        return "Ttc{" +
                "generalRange=" + generalRange +
                ", sightRange=" + sightRange +
                ", cartridge=" + cartridge +
                ", optics=" + optics +
                '}';
    }
}
