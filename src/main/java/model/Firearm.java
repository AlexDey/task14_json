package model;

import java.util.Arrays;

public class Firearm {

    private String model;
    private String handy;
    private String origin;
    private Ttc ttc;
    private String[] material;

    public Firearm() {
    }

    public Firearm(String model, String handy, String origin, Ttc ttc, String[] material) {
        this.model = model;
        this.handy = handy;
        this.origin = origin;
        this.ttc = ttc;
        this.material = material;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getHandy() {
        return handy;
    }

    public void setHandy(String handy) {
        this.handy = handy;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Ttc getTtc() {
        return ttc;
    }

    public void setTtc(Ttc ttc) {
        this.ttc = ttc;
    }

    public String[] getMaterial() {
        return material;
    }

    public void setMaterial(String[] material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Firearm{" +
                "model='" + model + '\'' +
                ", handy='" + handy + '\'' +
                ", origin='" + origin + '\'' +
                ", ttc=" + ttc +
                ", material=" + (material == null ? null : Arrays.asList(material)) +
                '}';
    }
}
